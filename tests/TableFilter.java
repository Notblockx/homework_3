package domashka3.tests;

import domashka3.DriverInit;
import domashka3.EventHandler;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class TableFilter extends DriverInit {

    //константи для кольорового виводу
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_GREEN = "\u001B[32m";

    public static void main(String[] args) {

        EventFiringWebDriver driver = initDriver();
        driver.register(new EventHandler());

            // 1) вхід в адмін панель
            driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
            WebElement loginField = driver.findElement(By.name("email"));
            loginField.sendKeys("webinar.test@gmail.com");
            WebElement passField = driver.findElement(By.name("passwd"));
            passField.sendKeys("Xcg7299bnSmMuRLp9ITw");
            WebElement enter = driver.findElement(By.name("submitLogin"));
            enter.click();
            (new WebDriverWait(driver, 10)).until(ExpectedConditions.titleIs("Dashboard • prestashop-automation"));

            // 2) клік на підменю "категорії"
            WebElement catalog = driver.findElement(By.id("subtab-AdminCatalog"));
            WebElement categories = driver.findElement(By.id("subtab-AdminCategories"));
            Actions builder = new Actions(driver);
            builder.moveToElement(catalog).moveToElement(categories).click(categories).build().perform();
            //System.out.println(driver.getTitle());

            // 3) додати нову категорію
            WebElement addCat = driver.findElement(By.id("page-header-desc-category-new_category"));
            addCat.click();

            // 4) створення і збереження нової категорії
            WebElement textField = driver.findElement(By.id("name_1"));
            textField.sendKeys("OmegaLul");
            WebElement saveBtn = driver.findElement(By.id("category_form_submit_btn"));
            saveBtn.click();
            WebElement successMsg = driver.findElement(By.className("alert-success"));
            System.out.println(ANSI_GREEN + "Is success message displayed? - " + ANSI_RESET + successMsg.isDisplayed());

            // 5) фільтр таблиці по  імені
            WebElement nameFilter = driver.findElement(By.xpath("//input[@name = \"categoryFilter_name\"]"));
            nameFilter.sendKeys("OmegaLul");
            WebElement findBtn = driver.findElement(By.id("submitFilterButtoncategory"));
            findBtn.click();
            WebElement baseTable = driver.findElement(By.xpath("//table[@id=\"table-category\"]/tbody"));
            List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));
            System.out.println(ANSI_GREEN + "Is new category written? - " + ANSI_RESET + tableRows.get(0).getText().contains("OmegaLul"));
            driver.quit();
        }
  }
