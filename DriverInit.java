package domashka3;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class DriverInit {

    public static EventFiringWebDriver initDriver() {
        WebDriverManager.chromedriver().setup();
        return new EventFiringWebDriver(new ChromeDriver());
    }
}
